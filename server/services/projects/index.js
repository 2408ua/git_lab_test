/**
 * @param axios {Object}
 * @param user {String}
 * @param client {Object}
 * @return Array | Object
 */
const getProjects = async (axios, user, client) => {
  try {
    const key = process.env.REDIS_KEY + user;
    const projects = await client.getEX(key);

    if (projects) return projects;

    const {data} = await axios.get(process.env.GITLAB_URL + `/users/${user}/projects`);
    const value = data.map(item => ({
      name: item.name,
      description: item.description,
      web_url: item.web_url,
      readme_url: item.readme_url,
      http_url_to_repo: item.http_url_to_repo,
    }));
    client.setEX(key, value);

    return value;

  } catch (err) {
    return {err, status: 503};
  }
};

module.exports = {
  getProjects,
}