/**
 * @param axios {Object}
 * @param user {String}
 * @return Array | Object
 */
const getUser = async (axios, user) => {
  try {
    const {data} = await axios.get(process.env.GITLAB_URL + `/users?username=${user}`);

    return data;

  } catch (err) {
    return {err, status: 503};
  }
};

module.exports = {
  getUser,
}