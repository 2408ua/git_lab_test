const redis = require("redis");
const client = redis.createClient();
const util = require('util');

client.get = util.promisify(client.get);

/**
 * @param key {String}
 * @param ttl {Number} expire time in seconds
 */
client.getEX = async (key, ttl = process.env.REDIS_TTL) => {
  const value = await client.get(key);
  return !value ? value : client.expire(key, ttl) && JSON.parse(value);
}

/**
 * @param key {String}
 * @param value {Object}
 * @param ttl {Number} expire time in seconds
 */
client.setEX = (key, value, ttl = process.env.REDIS_TTL) => {
  client.set(key, JSON.stringify(value), 'EX', ttl);
}

client.on("error", error => console.error(error));

module.exports = client;