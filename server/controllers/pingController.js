const errorHandler = require('../helpers/errorHandler');

/**
 * @desc GET ping answer
 * @router /test/ping
 */
const getPong = async (req, res) => {
  try {
    res.json({data: `pong`});

  } catch (err) {
    errorHandler(res, {err, status: 500});
  }
};

module.exports = {
  getPong,
}