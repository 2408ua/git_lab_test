const service = require('../services/projects');
const errorHandler = require('../helpers/errorHandler');

/**
 * @desc GET public projects by user name
 * @router /gitlab/projects
 */
const getProjects = async (req, res) => {
  try {
    const {user} = req.query;
    const client = req.client;
    const axios = req.axios;
    const projectsData = await service.getProjects(axios, user, client);
    if (projectsData.err) return errorHandler(res, projectsData);

    res.json(projectsData);

  } catch (err) {
    errorHandler(res, {err, status: 500});
  }
};

module.exports = {
  getProjects,
}