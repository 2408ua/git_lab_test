/**
 * @param res {Object}
 * @param error {Object}
 */
module.exports = (res, error) => {
  const {status, err} = error;
  const settings = {
    showErrorOnClient: process.env.NODE_ENV === 'development',
    showErrorOnServerConsole: process.env.NODE_ENV === 'development',
  };

  const messages = {
    '400': 'Bad Request!',
    '404': 'Not found!',
    '409': 'Conflict!',
    '422': 'Unprocessable Entity!',
    '500': 'Server error!',
    '503': 'Service Unavailable! Try later.',
  };
  const answer = {errors: [{msg: `${err.msg || messages[status]}`}]};

  if (settings.showErrorOnClient && !err.msg) answer.consoleError = err.toString();
  if (settings.showErrorOnServerConsole) console.log(err, '\n', err.toString());

  res.status(status).send(answer);
};





