require('dotenv').config();
const express = require('express');
const router = express.Router();
const routes = require('./routes');
const client = require('./config/cache');
const axios = require('axios');
const app = express();
const PORT = process.env.PORT || 443;

router.use((req, res, next) => {
  req.client = client;
  req.axios = axios;
  next();
})

app.use('/', routes(router));

if (process.env.NODE_ENV === 'development') {
  app.listen(PORT, () => console.log(`Server running on port: ${PORT}`));
}
