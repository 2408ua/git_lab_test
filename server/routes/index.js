const ping = require('../controllers/pingController');
const gitLab = require('../controllers/gitLabController');
const isUserExist = require('../validation/checkUserExist');
const Validate = require('../validation');

module.exports = router => {
  // PING
  router.get('/test/ping', ping.getPong);

  //GITLAB
  router.get('/gitlab/projects', Validate.getProjects, isUserExist, gitLab.getProjects);

  return router;
}