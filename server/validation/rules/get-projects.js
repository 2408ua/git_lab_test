const {query} = require('express-validator');

module.exports = [
  query('user', 'User name is required').not().isEmpty(),
];