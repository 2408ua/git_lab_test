const validate = require('./validate');

module.exports = {
  getProjects: validate(require('./rules/get-projects')),
}