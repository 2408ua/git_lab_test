const errorHandler = require('../helpers/errorHandler');
const service = require('../services/users');

/**
 * @desc check user exist middleware
 * @param req {Object}
 * @param res {Object}
 * @param next {Function}
 */
module.exports = async (req, res, next) => {
  try {
    const {user} = req.query;
    const axios = req.axios;
    const userData = await service.getUser(axios, user);
    if (!userData.length) return errorHandler(res, {status: 404, err: {msg: `User with name ${user} not found!`}});

    next();

  } catch (err) {
    errorHandler(res, {err, status: 500});
  }
}